'use strict';
var mongoose = require('mongoose'),
  Sensor = mongoose.model('Sensors');

exports.list_all_records = function(req, res) {
  console.log(req.query);
  var limit = req.query.limit || 10;
  if (req.query.sort=='desc') {
    if (limit) {
      Sensor.find({}).sort({Created_date: 'desc'}).limit(parseInt(limit)).exec(function(err,record) {
        if (err)
          res.send(err);
        res.json(record);
      });
    } 
    else {
      Sensor.find({}).sort({Created_date: 'desc'}).exec(function(err,record) {
        if (err)
          res.send(err);  
        res.json(record);
      });
    }

  } else {
    if (limit) {
      Sensor.find({}, function(err, record) {
        if (err)
          res.send(err);
        res.json(record);
      });
    } else {
      Sensor.find({}).limit(parseInt(limit)).exec(function(err, record) {
        if (err)
          res.send(err);
        res.json(record);
      });
    }
  }
};

exports.create_a_record = function(req, res) {
  var new_Sensor = new Sensor(req.body);
  new_Sensor.save(function(err, record) {
    if (err)
      res.send(err);
    res.json(record);
  });
};


exports.read_a_record = function(req, res) {
  Sensor.findById(req.params.sensorId, function(err, record) {
    if (err)
      res.send(err);
    res.json(record);
  });
};


exports.update_a_record = function(req, res) {
  Sensor.findOneAndUpdate({_id: req.params.sensorId}, req.body, {new: true}, function(err, record) {
    if (err)
      res.send(err);
    res.json(record);
  });
};


exports.delete_a_record = function(req, res) {
  Sensor.remove({
    _id: req.params.sensorId
  }, function(err, record) {
    if (err)
      res.send(err);
    res.json({ message: 'Record successfully deleted' });
  });
};
